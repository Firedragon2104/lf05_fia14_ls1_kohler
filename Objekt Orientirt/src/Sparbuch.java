
public class Sparbuch {
	private int kontonummer;
	private double kapital;
	private double zinssatz;

	public void zahleEin(int geld) {
		this.kapital = this.kapital + geld;
		System.out.println(this.kapital);
	}

	public void hebeAb(int geld) {
		this.kapital = this.kapital - geld;
		System.out.println(this.kapital);
	}

	public double getErtrag(int jahr) {
		zinssatz=zinssatz/100;
		this.kapital = this.kapital * (Math.pow(zinssatz, jahr));
		return kapital;
	}

	public void verzinse() {
		zinssatz=zinssatz/100;
		this.kapital = this.kapital * (Math.pow(zinssatz, 1));
	}

	public int getKontonummer() {
		return kontonummer;
	}

	public double getKapital() {
		return kapital;
	}

	public double getZinssatz() {
		return zinssatz;
	}

	public Sparbuch(int konto, int kap, double zins) {
		this.kontonummer = konto;
		this.kapital = kap;
		this.zinssatz = zins;
	}

}
