public class Konfiguration {
	public static void main (String[]args) {
	int euro;
	int cent;
	int summe;
	double patrone = 46.24;
	String typ = "Automat AVR";
	char sprachModul = 'd';
	boolean status;
	String bezeichnung = "Q2021_FAB_A";
	String name;
	final byte PRUEFNR = 4; 
	double prozent;
	double maximum = 100.00;
	int muenzenEuro = 130;
	int muenzenCent = 1280;
	
	name = typ + " " + bezeichnung;
	System.out.println("Name: " + name);
	prozent = maximum - patrone;
	summe = muenzenCent + muenzenEuro * 100;
	euro = summe / 100;
	cent = summe % 100;
	
	System.out.println("Sprache: " + sprachModul);
	System.out.println("Pruefnummer : " + PRUEFNR);
	System.out.println("Fuellstand Patrone: " + prozent + " %");
	System.out.println("Summe Euro: " + euro +  " Euro");
	System.out.println("Summe Rest: " + cent +  " Cent");	
	status = (euro <= 150)&& (sprachModul == 'd')&& (cent != 0)&& (euro >= 50)&& (prozent >= 50.00) &&  (!(PRUEFNR == 5 || PRUEFNR == 6));
	System.out.println("Status: " + status);
	}
}