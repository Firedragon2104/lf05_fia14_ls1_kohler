﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
    	Scanner tasttur = new Scanner(System.in);
       double rückgabebetrag;
       double zuZahlen;

       zuZahlen = fahrkartenbestellungErfassen();
       rückgabebetrag = fahrkartenBezahlen(zuZahlen);
       fahrkartenausgabe();
       rueckgeldAusgabe(rückgabebetrag);
    }

    
    public static double fahrkartenbestellungErfassen() {
    	Scanner tastatur = new Scanner(System.in);
    	double zuZahlen;
    	System.out.println("Ein Ticket kostet: 1,50€");
    	System.out.println("Wie viele Tickets wollen Sie kaufen?");
    	zuZahlen = tastatur.nextDouble();
    	if (zuZahlen <1 || zuZahlen >10) {
    		System.out.println("SIE HABEN EINE FALSCHE ANZAHL EINGEGEBEN ES WIRD NUN NUR EIN TICKET GEDRUCKT");
    	zuZahlen = 1;
    	}
    	zuZahlen = zuZahlen*1.50;
    	return zuZahlen;
    }
    public static double fahrkartenBezahlen(double zuZahlen) {
    	Scanner tastatur = new Scanner(System.in);
    	double eingezahlterGesamtbetrag = 0.00;
    	double eingeworfeneMünze;
    	double rückgabebetrag;

         while(eingezahlterGesamtbetrag < zuZahlen)
         {
      	   System.out.printf("Noch zu zahlen: " + "%.2f" + " Euro" + "\n",(zuZahlen - eingezahlterGesamtbetrag)); 
      	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
      	   eingeworfeneMünze = tastatur.nextDouble();
             eingezahlterGesamtbetrag += eingeworfeneMünze;
         }
         tastatur.close();
    	rückgabebetrag = eingezahlterGesamtbetrag - zuZahlen;
    	return rückgabebetrag;
    }
    public static void fahrkartenausgabe() {
    	 System.out.println("\nFahrschein wird ausgegeben");
         for (int i = 0; i < 8; i++)
         {
            System.out.print("=");
            try {
  			Thread.sleep(250);
  		} catch (InterruptedException e) {
  			// TODO Auto-generated catch block
  			e.printStackTrace();
  		}
         }
    }
    public static void rueckgeldAusgabe(double rückgabebetrag) {
    	 if(rückgabebetrag > 0.00)
         {
      	   System.out.printf("Der Rückgabebetrag in Höhe von " + "%.2f" + " EURO ",rückgabebetrag);
      	   System.out.println("wird in folgenden Münzen ausgezahlt:");

             while(rückgabebetrag >= 2.00) // 2 EURO-Münzen
             {
          	  System.out.println("2 EURO");
  	          rückgabebetrag -= 2.00;
             }
             while(rückgabebetrag >= 1.00) // 1 EURO-Münzen
             {
          	  System.out.println("1 EURO");
  	          rückgabebetrag -= 1.00;
             }
             while(rückgabebetrag >= 0.50) // 50 CENT-Münzen
             {
          	  System.out.println("50 CENT");
  	          rückgabebetrag -= 0.50;
             }
             while(rückgabebetrag >= 0.20) // 20 CENT-Münzen
             {
          	  System.out.println("20 CENT");
   	          rückgabebetrag -= 0.20;
             }
             while(rückgabebetrag >= 0.10) // 10 CENT-Münzen
             {
          	  System.out.println("10 CENT");
  	          rückgabebetrag -= 0.10;
             }
             while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
             {
          	  System.out.println("5 CENT");
   	          rückgabebetrag -= 0.05;
             }
         }
    	 System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
    }
    
}